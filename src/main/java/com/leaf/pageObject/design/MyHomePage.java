package com.leaf.pageObject.design;

import com.autoBot.testng.api.base.Annotations;

public class MyHomePage extends Annotations{
	
	public MyHomePage() {}
	
	public LeadPage clickLead() {
		click(locateElement("Link", "Leads"));
		
		return new LeadPage();
	}
	
		}
		


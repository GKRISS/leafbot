package com.leaf.pageObject.design;

import com.autoBot.testng.api.base.Annotations;

public class HomePage extends Annotations{

	public HomePage() {}

	public MyHomePage clickCrmsfa() {
		click(locateElement("link", "CRM/SFA"));
		return new MyHomePage();
}
}



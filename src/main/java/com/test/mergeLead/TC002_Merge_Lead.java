package com.test.mergeLead;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.testng.api.base.Annotations;
import com.leaf.pageObject.design.LoginPage;

public class TC002_Merge_Lead extends Annotations{

	@BeforeTest
	public void setData() {
		excelFileName = "TC001";
		testcaseName = "TC002_Merge_Lead";
		testcaseDec = "Merge Lead ";
		author = "GOPAL";
		category = "smoke";
}
	@Test(dataProvider = "fetchData")
	public void login (String un, String pwd) {
		
		new LoginPage()
		.enterUserName(un)
		.enterPassWord(pwd)
		.clickLogin();
		
}
}
